package xyz.shpasha.androidtfs.utils.validation;

public interface Validator<T> {
    boolean isValid(T value);
    String getDescription();
}
