package xyz.shpasha.androidtfs.utils.validation;

import java.util.regex.Pattern;

public class DateValidator implements Validator<String> {

    private static final Pattern pattern
            = Pattern.compile("^(0[1-9]|[12]\\d|3[01])\\.(0[1-9]|1[0-2])\\.([12]\\d{3})$");

    @Override
    public boolean isValid(String value) {
        return pattern.matcher(value).matches();
    }

    @Override
    public String getDescription() {
        return "Дата не в формате ДД.ММ.ГГГГ!";
    }
}
