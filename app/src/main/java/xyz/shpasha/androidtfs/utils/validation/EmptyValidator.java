package xyz.shpasha.androidtfs.utils.validation;

import android.text.TextUtils;

public class EmptyValidator implements Validator<String> {

    @Override
    public boolean isValid(String value) {
        return !TextUtils.isEmpty(value);
    }

    @Override
    public String getDescription() {
        return "Значение не может быть пустым";
    }
}