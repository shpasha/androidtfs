package xyz.shpasha.androidtfs.utils;

import java.util.HashMap;
import java.util.Map;

public class ErrorTranslator {

    private static Map<String, String> map = new HashMap<String, String>() {
        {
            put("Unable to resolve", "При загрузке данных произошла ошибка. Проверьте Ваше подключение к сети.");
            put("You used all your", "Вы использовали все свои попытки");
        }
    };

    public static String translateError(String message) {
        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (message.contains(entry.getKey())) {
                return entry.getValue();
            }
        }
        return message;
    }
}
