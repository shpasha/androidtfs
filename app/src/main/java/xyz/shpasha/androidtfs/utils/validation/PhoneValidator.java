package xyz.shpasha.androidtfs.utils.validation;

import android.util.Patterns;

public class PhoneValidator implements Validator<String> {

    @Override
    public boolean isValid(String value) {
        return Patterns.PHONE.matcher(value).matches();
    }

    @Override
    public String getDescription() {
        return "Некорректный номер телефона";
    }
}
